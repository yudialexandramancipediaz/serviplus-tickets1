import './App.css';
import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
//import UserTable from './components/UserTable';

//import Navigator from './components/Navigator';

import ParkingList from './components/ParkingList';
import ParkingCrear from './components/ParkingCrear';
import ParkingEditar from './components/ParkingEditar';

function App() {
  return (
    <Router>
        <Routes>
            <Route path='/' element = {<ParkingList/>}/>
            <Route path='/crear' element = {<ParkingCrear/>}/>
            <Route path='/editar/:id' element = {<ParkingEditar/>}/>
        </Routes>
    </Router>
  );
}

export default App;


