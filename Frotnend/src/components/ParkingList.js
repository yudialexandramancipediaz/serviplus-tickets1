import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import imageParking from '../../src/assets/img/parking.jpg';
import React, { useState, useEffect } from "react";

import { Link } from 'react-router-dom';

const URI = "http://localhost:4100/";

const CompParkingList = () => {
  const [params, setParams] = useState([]);
  useEffect(() => {
    getRutasvuelos();
  }, []);


  //Creaciópn de Metodos del componente
  //Metodo para listar las rutas de vuelo
  const getRutasvuelos = async () => {
    const res = await axios.get(`${URI}buscarall`);
  
    setParams(res.data['result']);
    console.log(res.data['result']);
  };

  
  //Método para eliminar
  const deleteRutaVuelo = async (id) => {
    axios.delete(`${URI}eliminar/${id}`)
    getRutasvuelos()
  }

  return (

    <div className="container">
      <nav className="navbar navbar-dark" style={{background:"#e3f2fd"}}>
        <div className="container-fluid">
          <a className="navbar-brand" href="/" style={{color:"black"}}>
            <img  src={imageParking} alt="consultorio1" width="30%" height="24%"/>
            <b style={{marginTop:'20%'}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARKING - SERVIPLUS</b>  
          </a>
        </div>
      </nav>
      <div className="card-body">
        <br/>
        <Link to = {'crear'} className='btn btn-dark mt-2 ml-2 mb-1' style={{width:"17%" }}>Registar Usuario</Link>
        <br/><br/>
        <table className="table table-striped  table-sm table-responsive">
          <thead style={{ color: "#FDFBFD", background: "#0F0F0E" }}>
            <tr style={{ textAlign: 'center' }}>
              <th style={{ width: "36%" }}><b>USERNAME</b></th>
              <th style={{ width: "36%" }}><b>IDENTIFICACION</b></th>
              <th style={{ width: "12%" }}><b>NOMBRE</b></th>
              <th style={{ width: "16%" }}><b></b></th>
            </tr>
          </thead>
          <tbody>
            {params.map(user => (
              <tr key={user._id} className="table-active" style={{ textAlign: 'center', color: "#0F0F0E", background: "#FDFBFD" }}>
                <td><b>{user.username}</b></td>
                <td><b>{user.identificacion}</b></td>
                <td><b>{user.datosPersonales.nombre} {user.datosPersonales.apellidos}</b></td>
                <td style={{ alignItems: 'center' }}>
                  <Link to = {`/editar/${user._id}`} className="fa-regular fa-pen-to-square"></Link>
                  &nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;
                  <Link onClick={() => deleteRutaVuelo(user._id)} className="fa-solid fa-trash" style={{ color: "red" }}></Link>
                </td>
              </tr>
            ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default CompParkingList;
