import axios from 'axios';
import {useEffect,useState} from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import { Link } from 'react-router-dom';
import imageParking from "../../src/assets/img/parking.jpg";

const URI = 'http://localhost:4100/'

const CompParkingEdit = ()=>{
    const navigate = useNavigate();
    const {id} = useParams();
    const [username,setUsername]= useState('')
    const [identificacion, setIdentificacion] = useState('')
    const [nombre,setNombre]= useState('')
    const [apellidos, setApellidos] = useState('')
    const [correo,setCorreo]= useState('')
    
   
    //METODO EDITAR
    const update = async (e)=>{
        const data = {
          username: username,
          identificacion: identificacion,
          datosPersonales: {
            nombre: nombre,
            apellidos: apellidos,
            correo: correo
          }
        };
        e.preventDefault();
        await axios.put(`${URI}actualizar/${id}`, data)
        //RESULT STATUS 200
        navigate('/');
    }

    useEffect(()=>{
      getRutaById()
    },[])


    const getRutaById = async()=>{
      const res = await axios.get(`${URI}buscar/${id}`);
      console.log('Elemento Buscado-->',res.data)
      setUsername(res.data['message'].username)
      setIdentificacion(res.data['message'].identificacion)
      setNombre(res.data['message'].datosPersonales.nombre)
      setApellidos(res.data['message'].datosPersonales.apellidos)
      setCorreo(res.data['message'].datosPersonales.correo)
  }
    return (
            <div className="container">
                    <nav className="navbar navbar-dark" style={{ background: "#e3f2fd" }}>
                  <div className="container-fluid">
                    <a className="navbar-brand" href="/" style={{ color: "black" }}>
                      <img
                        src={imageParking}
                        alt="consultorio1"
                        width="30%"
                        height="24%"
                      />
                      <b style={{ marginTop: "20%", color: "#0F0F0E" }}>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARKING - SERVIPLUS
                      </b>
                    </a>
                  </div>
                </nav>
                <br />
                <div className="container-fluid" style={{marginLeft:'21%'}}>
                    <a className="navbar-brand" href="/" >
                        <b style={{marginTop:'20%', marginLeft:'1%', color:'#0F0F0E', fontSize:'140%'}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ACTUALIZACIÓN DE DATOS</b>  
                    </a>
                </div>
                <br />
              <div className="hold-transition login-page">
          
                <div className="hold-transition login-page">
                  <div className="login-box">
                    {/* /.login-logo */}
                    <div className="card">
                      <div className="card-body login-card-body">
                        <p className="login-box-msg">
                          <b style={{ color: "rgb(0, 128, 128)", maxWidth:'75%' }}>
                            Datos del usuario.
                          </b>
                        </p>
          
                        <form onSubmit={update}>
                          <div className="input-group mb-3">
                            <input type="text"
                              className="form-control"
                              placeholder="User"
                              id="username"
                              name="username"
                              value={username || ""}
                              onChange={(e)=>setUsername(e.target.value)}
                              style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                              color='#288CCA'
                              required
                            />
                            <div className="input-group-append">
                              <div className="input-group-text">
                                <span className="fas fa-user" style={{color:'black'}} />
                              </div>
                            </div>
                          </div>
          
                          <div className="input-group mb-3">
                            <input type="text"
                              className="form-control"
                              placeholder="Identificacion de usuario"
                              id="identificacion"
                              name="identificacion"
                              value={identificacion || ""}
                              onChange={(e)=>setIdentificacion(e.target.value)}
                              style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                              color='#288CCA'
                              required
                            />
                            <div className="input-group-append">
                              <div className="input-group-text">
                                <span className="fas fa-star" style={{color:'black'}} />
                              </div>
                            </div>
                          </div>
          
                          <div className="input-group mb-3">
                            <input type="text"
                              className="form-control"
                              placeholder="Nombre del usuario"
                              id="nombre"
                              name="nombre"
                              value={nombre || ""}
                              onChange={(e)=>setNombre(e.target.value)}
                              style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                              color='#288CCA'
                              required
                            />
                            <div className="input-group-append">
                              <div className="input-group-text">
                                <span className="fas fa-bars" style={{color:'black'}} />
                              </div>
                            </div>
                          </div>

                          <div className="input-group mb-3">
                            <input type="text"
                              className="form-control"
                              placeholder="Apellidos del usuario"
                              id="apellidos"
                              name="apellidos"
                              value={apellidos || ""}
                              onChange={(e)=>setApellidos(e.target.value)}
                              style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                              color='#288CCA'
                              required
                            />
                            <div className="input-group-append">
                              <div className="input-group-text">
                              <span className="fas fa-bars" style={{color:'black'}} />
                              </div>
                            </div>
                          </div>

                          <div className="input-group mb-3">
                            <input type="email"
                              className="form-control"
                              placeholder="Correo electronico"
                              id="correo"
                              name="correo"
                              value={correo || ""}
                              onChange={(e)=>setCorreo(e.target.value)}
                              style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                              color='#288CCA'
                              required
                            />
                            <div className="input-group-append">
                              <div className="input-group-text">
                                <span className="fas fa-envelope" style={{color:'black'}} />
                              </div>
                            </div>
                          </div>
          
                          <div className="social-auth-links text-center mb-3">
                            <button type="submit" className="btn btn-dark">
                              <b>Actualizar</b>
                            </button>&nbsp;&nbsp;&nbsp;
                            <Link to={"/"} className="btn btn-info">
                              <b>Regresar principal</b>
                            </Link>
                          </div>
                        </form>
                      </div>
                      {/* /.login-card-body */}
                    </div>
                  </div>
                  {/* /.login-box */}
                </div>
              </div>
            </div>
              
    );
}

export default CompParkingEdit;

