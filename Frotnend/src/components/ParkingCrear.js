import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import imageParking from "../../src/assets/img/parking.jpg";
import { Link } from 'react-router-dom';

const URI = "http://localhost:4100/crear/";

const CompParkingCrear = () => {
  const navigate = useNavigate();
  const [ params, setParams ] = useState({
    username: "",
    password: "",
    identificacion: "",
    nombre: "",
    apellidos: "",
    correo: ""
  })

  const { username, password, identificacion, nombre, apellidos, correo } = params;

  const onChange = (e) => {
    setParams({
      ...params,
      [e.target.name]: e.target.value,
    });
    console.log(params);
  };
  useEffect(() => {
    document.getElementById("password").focus();
  }, []);

  const insert = async (e) => {
    const data = {
      username: params.username,
      pass: params.password,
      identificacion: params.identificacion,
      datosPersonales: {
        nombre: params.nombre,
        apellidos: params.apellidos,
        correo: params.correo
      }
    };

    e.preventDefault();
  
    await axios.post(URI, data)
    navigate("/");
  };

  return (
  <div className="container">
        <nav className="navbar navbar-dark" style={{ background: "#e3f2fd" }}>
        <div className="container-fluid">
          <a className="navbar-brand" href="/" style={{ color: "black" }}>
            <img
              src={imageParking}
              alt="consultorio1"
              width="30%"
              height="24%"
            />
            <b style={{ marginTop: "20%", color: "#0F0F0E" }}>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PARKING - SERVIPLUS
            </b>
          </a>
        </div>
      </nav>
      <br />
      <div className="container-fluid" style={{marginLeft:'21%'}}>
          <a className="navbar-brand" href="/" >
              <b style={{marginTop:'20%', marginLeft:'1%', color:'#0F0F0E', fontSize:'140%'}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REGISTRO USUARIOS</b>  
          </a>
      </div>
      <br />
    <div className="hold-transition login-page">

      <div className="hold-transition login-page">
        <div className="login-box">
          {/* /.login-logo */}
          <div className="card">
            <div className="card-body login-card-body">
              <p className="login-box-msg">
                <b style={{ color: "rgb(0, 128, 128)", maxWidth:'75%' }}>
                  Ingrese los datos del nuevo usuario.
                </b>
              </p>

              <form onSubmit={insert}>
                <div className="input-group mb-3">
                  <input type="text"
                    className="form-control"
                    placeholder="User"
                    id="username"
                    name="username"
                    value={username}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-user" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="input-group mb-3">
                  <input type="password"
                    className="form-control"
                    placeholder="Password de usuario"
                    id="password"
                    name="password"
                    value={password}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-unlock" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="input-group mb-3">
                  <input type="text"
                    className="form-control"
                    placeholder="Identificacion de usuario"
                    id="identificacion"
                    name="identificacion"
                    value={identificacion}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-star" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="input-group mb-3">
                  <input type="text"
                    className="form-control"
                    placeholder="Nombre del usuario"
                    id="nombre"
                    name="nombre"
                    value={nombre}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-bars" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="input-group mb-3">
                  <input type="text"
                    className="form-control"
                    placeholder="Apellidos del usuario"
                    id="apellidos"
                    name="apellidos"
                    value={apellidos}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-bars" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="input-group mb-3">
                  <input type="email"
                    className="form-control"
                    placeholder="Correo electronico"
                    id="correo"
                    name="correo"
                    value={correo}
                    onChange={onChange}
                    style={{color:'#288CCA', maxWidth:'75%', marginLeft:'10%'}}
                    color='#288CCA'
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <span className="fas fa-envelope" style={{color:'black'}} />
                    </div>
                  </div>
                </div>

                <div className="social-auth-links text-center mb-3">
                  <button type="submit" className="btn btn-dark">
                    <b>Registrar</b>
                  </button>&nbsp;&nbsp;&nbsp;
                  <Link to={"/"} className="btn btn-info">
                    <b>Regresar principal</b>
                  </Link>
                </div>
              </form>
            </div>
            {/* /.login-card-body */}
          </div>
        </div>
        {/* /.login-box */}
      </div>
    </div>
  </div>
    
  );
};

export default CompParkingCrear;
