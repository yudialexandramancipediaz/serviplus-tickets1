//las importaciones de nuestro proyecto

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
const { Types } = require("mongoose");
//methodOverride = require("method-override");
//var mongoose = require('mongoose');
app.use(express.json());
app.use(express.urlencoded({
extended: true
   }
));

// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Headers', 'Authorization, X-API-KEY, origin, X-Requestd-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT, DELETE');
    res.header('Allow', 'GET,POST,OPTIONS,PUT, DELETE');
    next();
});
    app.use(require('./routers/routers'))
    module.exports = app;