//router.js dentro de la carpeta routers en el cual estarán las rutas de nuestro proyecto 

const { Router } = require('express');
const router = Router();

//DEFINIR LOS ACCESOS A LOS CONTROLLERS
var loginController = require('../src/controllers/loginController');
var parametrosController = require('../src/controllers/parametrosController');
var serviciosController = require('../src/controllers/serviciosController');

//DEFINIR LOS ENDPOINT
//ENDPOINT LOGIN
router.get('/prueba',loginController.testLogin)
router.post('/crear',loginController.guardarLogin);
router.get('/buscar/:id',loginController.buscarData);
router.get('/buscarall/:id?', loginController.listarAllData);
router.put('/actualizar/:id', loginController.updateLogin);
router.delete('/eliminar/:id',loginController.deleteLogin);
//ENDPOINT PARAMETROS
router.get('/prueba',parametrosController.testParametro);
router.post('/crearparametro',parametrosController.guardarParametro);
router.get('/buscarparametro/:id',parametrosController.buscarData);
router.get('/buscarparametros/:id?', parametrosController.listarAllData);
router.put('/actualizarparametro/:id',parametrosController.updateParametro);
router.delete('/eliminaroarametro/:id',parametrosController.deleteParametro);
//ENDPOINT SERVICIOS
router.get('/prueba', serviciosController.testServicio);
router.post('/crearservicio',serviciosController.guardarServicio);
router.get('/buscarservicio/:id',serviciosController.buscarData);
router.get('/buscarservicios/:id?', serviciosController.listarAllData);
router.put('/actualizarservicio/:id', serviciosController.updateServicio);
router.delete('/eliminarservicio/:id',serviciosController.deleteServicio);

module.exports=router;