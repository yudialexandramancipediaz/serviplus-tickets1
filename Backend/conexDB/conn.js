const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/parkingDB",{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    
},(err,res)=>{
    if(err){
        throw err;
    }else{
        console.log('La conexion a la DB ha sido exitosa');
    }
});

module.exports = mongoose;
