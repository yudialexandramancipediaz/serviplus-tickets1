const Parametro = require("../models/parametrosModels");

function testParametro (Req, res){
    res.status(200).send({
    message: 'Prueba de ejecucion del Controlador'
    });
}

function guardarParametro (req, res){
    var myParamatro = new Parametro(req.body);
    myParamatro.save((error,result)=>{
        res.status(200).send({
            message:result
        });
    });
   // res.status(200).send({
    //message: 'Prueba de ejecucion del Controlador'
   // });
}

function buscarData(req,res){

    var idParametro = req.params.id;
    Parametro.findById(idParametro).exec((err,result)=>{
        if (err){
           res.status(500).send({message:'Error desde Backend al ejecutar la peticion'}) 
            }
        else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({message:result });
                 }
        }
    });
}

function listarAllData(req,res){
    var idParametro = req.params.id;
    if(!idParametro){
        var result = Parametro.find({}).sort('_id')
    } else {
        var result = Parametro.find({_id:idParametro}).sort('_id');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'_id'})
        } else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({result });
            }
        }

    });
}

function updateParametro(req,res){
    var idParametro = req.params.id;
    Parametro.findOneAndUpdate({ _id:idParametro},req.body,{new:true},
        function(err,Parametro){
            if(err)
                res.send(err)
            res.json(Parametro)
        });
}

function deleteParametro(req,res) {
    var idParametro = req.params.id;
    Parametro.findByIdAndDelete(idParametro,
    function(err,Parametro){
            if(err){
                return res.json(500,{
                    message:'Error al acceder a la ruta'
                })
            }
            return res.json(Parametro)

        });
    
}

//FIN


//EXPORT->
module.exports = {
    testParametro,
    guardarParametro,
    buscarData,
    listarAllData,
    updateParametro,
    deleteParametro

};
