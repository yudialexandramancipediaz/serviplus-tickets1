const Servicio = require("../models/serviciosModels");

function testServicio (Req, res){
    res.status(200).send({
    message: 'Prueba de ejecucion del Controlador'
    });
}

function guardarServicio (req, res){
    var myServicio = new Servicio(req.body);
    myServicio.save((error,result)=>{
        res.status(200).send({
            message:result
        });
    });
    //res.status(200).send({
   // message: 'Prueba de ejecucion del Controlador'
   // });
}

function buscarData(req,res){
    var idServicio = req.params.id;
    Servicio.findById(idServicio).exec((err,result)=>{
        if (err){
           res.status(500).send({message:'Error desde Backend al ejecutar la peticion'}) 
            }
        else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({message:result });
                 }
        }
    });
}
function listarAllData(req,res){
    var idServicio = req.params.id;
    if(!idServicio){
        var result = Servicio.find({}).sort('_id')
    } else {
        var result = Servicio.find({_id:idServicio}).sort('_id');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error desde Backend al ejecutar la peticion'})
        } else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({result });
            }
        }

    });
}

function updateServicio(req,res){
    var idServicio = req.params.id;
    Servicio.findOneAndUpdate({ _id:idServicio},req.body,{new:true},
        function(err,Servicio){
            if(err)
                res.send(err)
            res.json(Servicio)
        });
}

function deleteServicio(req,res) {
    var idServicio = req.params.id;
    Servicio.findByIdAndDelete(idServicio,
    function(err,Servicio){
            if(err){
                return res.json(500,{
                    message:'Error al acceder a la ruta'
                })
            }
            return res.json(Servicio)

        });
    
}

//EXPORT->
module.exports = {
    testServicio,
    guardarServicio,
    buscarData,
    listarAllData,
    updateServicio,
    deleteServicio

};
