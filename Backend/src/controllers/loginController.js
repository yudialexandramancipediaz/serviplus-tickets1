const Login = require("../models/loginModels");

function testLogin (Req, res){
    res.status(200).send({
    message: 'Prueba de ejecucion del Controlador'
    });
}

function guardarLogin (req, res){
    var myLogin = new Login(req.body);
    myLogin.save((error,result)=>{
        res.status(200).send({
            message:result
        });
    });
    //res.status(200).send({
    //message: 'Prueba de ejecucion del Controlador'
    //});
}

function buscarData(req,res){
    var idLogin = req.params.id;
    Login.findById(idLogin).exec((err,result)=>{
        if (err){
           res.status(500).send({message:'Error desde Backend al ejecutar la peticion'}) 
            }
        else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({message:result });
                 }
        }
    });
}

function listarAllData(req,res){
    var idLogin = req.params.id;
    if(!idLogin){
        var result = Login.find({}).sort('_id')
    } else {
        var result = Login.find({_id:idLogin}).sort('_id');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error desde Backend al ejecutar la peticion'})
        } else{
            if(!result){
                res.status(404).send({message:'Error en el envio de parametros (Error frontend)'})
            }
            else {
                res.status(200).send({result });
            }
        }

    });
}

function updateLogin(req,res){
    var idLogin = req.params.id;
    Login.findOneAndUpdate({ _id:idLogin},req.body,{new:true},
        function(err,Login){
            if(err)
                res.send(err)
            res.json(Login)
        });
}

function deleteLogin(req,res) {
    var idLogin = req.params.id;
    Login.findByIdAndDelete(idLogin,
    function(err,Login){
            if(err){
                return res.json(500,{
                    message:'Error al acceder a la ruta'
                })
            }
            return res.json(Login)
        });
    
}

//EXPORT->
module.exports = {
    testLogin,
    guardarLogin,
    buscarData,
    listarAllData,
    updateLogin,
    deleteLogin

};
