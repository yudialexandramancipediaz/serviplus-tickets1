mongoose=require('mongoose');

var Schema = mongoose.Schema;

var parametroSchema=Schema({
       // _id: String,
        valor_moto_minuto: Number,
        valor_carro_minuto:Number,
        valor_cicla_minuto: Number,
        valor_otros: Number,
        nombre_parqueadero: String
      
    })

const parametros = mongoose.model('parametro',parametroSchema);
module.exports = parametros;