mongoose=require('mongoose');

var Schema = mongoose.Schema;

var servicioSchema=Schema({
   // _id:String,
    vehiculo: {
    placa:String,
    tipo_vehiculo:String
    },
    hora_entrada:String,
    hora_salida:String,
    fecha:String,
    estado:String,
    valor_servicio:String
})

const servicios = mongoose.model('servicio',servicioSchema);
module.exports = servicios;