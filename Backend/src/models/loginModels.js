 mongoose=require("mongoose");

var Schema = mongoose.Schema;

var loginSchema=Schema({
   // id:String,
    username:String,
    pass:String,
    identificacion:String,
    datosPersonales:{
        nombre:String,
        apellidos:String,
        correo:String
    }
});

const login = mongoose.model('login',loginSchema);
module.exports = login;